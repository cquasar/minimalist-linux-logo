# Minimalist Linux Logo Brand Guidelines

## Introduction

The Minimalist Linux Logo is a simple, modern interpretation of Linux's iconic penguin mascot. The logo is designed to be clean, easy to recognize, and versatile enough to work in a variety of contexts.

These brand guidelines are intended to provide clear guidelines for using the Minimalist Linux Logo and related assets in a way that is consistent with the brand's values and aesthetic.

## Logo Usage

The Minimalist Linux Logo is a key part of the brand identity and should be used consistently across all channels. When using the logo, please follow these guidelines:

- Always use the official version of the logo, available in the `./source-files` folder of this repository.
- Do not modify or alter the logo in any way.
- The logo should always be surrounded by a clear space of at least one-third the height of the logo, on its vertical variant, and one-third the width on its horizontal variant.
- The logo should always be displayed in its original colors, as defined in the color palette.

## Typography

The Minimalist Linux Logo uses the **Libertine Bold** for the logo and **Inter** font for all other text-based branding. The **Inter** font is modern, clean, and easy to read, making it a great choice for a minimalist brand identity.

When using text in conjunction with the Minimalist Linux Logo, please use the following guidelines:

- Apart from the logo, the primary font used for brand materials should be **Inter**.
- Use bold or italic versions of the font sparingly, and only for emphasis.
- Use a font size that is appropriate for the context and are easily legible, even at a distance.
- Do not use any other fonts in conjunction with the primary font, as this can detract from the brand's consistency.

## Iconography

The Minimalist Linux Logo uses a simple penguin's foot as its primary brand icon. This icon is intended to be used in contexts where the full logo is not necessary or appropriate.

When using the icon, please follow these guidelines:

- Always use the official version of the icon, available in the `./source-files` folder of this repository.
- Do not modify or alter the icon in any way.
- The icon should always be displayed in its original colors, as defined in the color palette.
- The icon should always be surrounded by a clear space of at least one-third the height of the icon.


## Color Palette

The Minimalist Linux Logo uses a simple color palette to create a modern, friendly feel. Consistent use of color is important for establishing brand recognition and creating a cohesive visual identity. The following color palettes should be used in all brand materials, including print and digital media.

### Primary Color

The primary color, ***Orange-Yellow***, should be used for all brand elements, such as logos, headlines, and call-to-action buttons.

- HEX: `#EEBC1D`
- RGB: `238, 188, 29`
- HSV: `47°, 88%, 93%`
- HSL: `47°, 84%, 53%`
- CMYK: `0%, 21%, 88%, 7%`

### Secondary Color

The secondary color, ***Onyx***, can be used to complement the primary color in supporting design elements, such as backgrounds or accents. It is great for texts on light backgrounds.

- HEX: `#33303E`
- RGB: `51, 48, 62`
- HSV: `257°, 22%, 24%`
- HSL: `257°, 13%, 20%`
- CMYK: `18%, 22%, 0%, 76%`

### Tertiary Color

The tertiary color, ***Cadet Blue (Crayola)***, should be used sparingly and only in situations where the primary and secondary colors are not appropriate.

- HEX: `#A6B6B8`
- RGB: `166, 182, 184`
- HSV: `185°, 10%, 72%`
- HSL: `185°, 11%, 66%`
- CMYK: `10%, 1%, 0%, 28%`

### Accent Color

The accent color, ***Cerulean Blue***, is an important element of the brand's color palette. It should be used sparingly and thoughtfully to draw attention to key elements and create visual interest, while not overwhelming the overall design.

The accent color can be used for elements such as call-to-action buttons, hyperlinks, and interactive elements to encourage user engagement and guide their attention.

- HEX: `#3362B6`
- RGB: `RGB(51, 98, 182)`
- HSL: `HSL(214, 55%, 45%)`
- CMYK: `CMYK(72%, 46%, 0%, 29%)`
- HSV: `HSV(214, 72%, 71%)`

### Reversed Color

The reversed color, ***Anti-Flash White***, can be used as a background color to create contrast with the primary color, or the logo text color on dark backgrounds. Can also be used on other text elements on dark backgrounds.

- HEX: `#F1F2F2`
- RGB: `RGB(241, 242, 242)`
- HSL: `HSL(180, 2%, 95%)`
- CMYK: `CMYK(0%, 0%, 0%, 5%)`
- HSV: `HSV(180, 0%, 95%)`

### Color Usage Guidelines

- Use the provided color codes for all brand materials.
- Maintain consistent color usage across all materials.
- Do not use colors outside of the specified palette.
- When using background colors, ensure they do not clash with the primary or secondary color palettes.
- Use color with purpose and intention, to draw attention to important elements and create a consistent user experience.

---

## Conclusion

By following these brand guidelines, we can ensure that the Minimalist Linux Logo is used consistently and effectively across all channels. If you have any questions or concerns about logo usage or branding, please create an issue on our [GitLab repository](https://gitlab.com/cquasar/minimalist-linux-logo) and we will answer as soon as possible.