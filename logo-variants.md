# Logo Variants

This page contains different variants of the minimalist Linux logo. Feel free to use these variants for non-commercial purposes. For commercial use, permission from Linus Torvalds is required.

## Variants

### Colored

#### Primary Color Vertical

![Minimalist Linux Logo - Primary Color Vertical](./source-files/vector/Inkscape/Vertical/linux-logo-primary-color.svg)

---

#### Primary Color Horizontal

![Minimalist Linux Logo - Primary Color Horizontal](./source-files/vector/Inkscape/Horizontal/linux-logo-primary-color.svg)

---

#### Secondary Color Vertical

![Minimalist Linux Logo - Secondary Color Vertical](./source-files/vector/Inkscape/Vertical/linux-logo-secondary-color.svg)

---

#### Secondary Color Horizontal

![Minimalist Linux Logo - Secondary Color Horizontal](./source-files/vector/Inkscape/Horizontal/linux-logo-secondary-color.svg)

---

#### Tertiary Color Vertical

![Minimalist Linux Logo - Tertiary Color Vertical](./source-files/vector/Inkscape/Vertical/linux-logo-tertiary-color.svg)

---

#### Tertiary Color Horizontal

![Minimalist Linux Logo - Tertiary Color Horizontal](./source-files/vector/Inkscape/Horizontal/linux-logo-tertiary-color.svg)

---

#### Reversed Vertical

![Minimalist Linux Logo - Reversed Vertical](./source-files/vector/Inkscape/Vertical/linux-logo-reversed.svg)

---

#### Reversed Horizontal

![Minimalist Linux Logo - Reversed Horizontal](./source-files/vector/Inkscape/Horizontal/linux-logo-reversed.svg)

---

### Alternatives

#### Monochrome Black Vertical

![Minimalist Linux Logo - Monochrome White Vertical](./source-files/vector/Inkscape/Vertical/linux-logo-monochrome-black.svg)

---

#### Monochrome Black Horizontal

![Minimalist Linux Logo - Monochrome White Horizontal](./source-files/vector/Inkscape/Horizontal/linux-logo-monochrome-black.svg)

---

#### Monochrome White Vertical

![Minimalist Linux Logo - Monochrome White Vertical](./source-files/vector/Inkscape/Vertical/linux-logo-monochrome-white.svg)

---

#### Monochrome White Horizontal

![Minimalist Linux Logo - Monochrome White Horizontal](./source-files/vector/Inkscape/Horizontal/linux-logo-monochrome-white.svg)

---

#### Greyscale Vertical

![Minimalist Linux Logo - Greyscale Vertical](./source-files/vector/Inkscape/Vertical/linux-logo-greyscale.svg)

---

#### Greyscale Horizontal

![Minimalist Linux Logo - Greyscale Horizontal](./source-files/vector/Inkscape/Horizontal/linux-logo-greyscale.svg)

---


### Icon only

#### Icon Primary Color

![Minimalist Linux Logo - Icon Primary Color](./source-files/vector/Inkscape/Icon/linux-logo-icon-primary-color.svg)

---

#### Icon Secondary Color

![Minimalist Linux Logo - Icon Secondary Color](./source-files/vector/Inkscape/Icon/linux-logo-icon-secondary-color.svg)

---

#### Icon Tertiary Color

![Minimalist Linux Logo - Icon Tertiary Color](./source-files/vector/Inkscape/Icon/linux-logo-icon-tertiary-color.svg)

---

#### Icon Monochrome Black

![Minimalist Linux Logo - Icon Monochrome Black](./source-files/vector/Inkscape/Icon/linux-logo-icon-monochrome-black.svg)

---

#### Icon Monochrome White

![Minimalist Linux Logo - Icon Monochrome White](./source-files/vector/Inkscape/Icon/linux-logo-icon-monochrome-white.svg)

---

#### Icon Reversed

![Minimalist Linux Logo - Icon Reversed](./source-files/vector/Inkscape/Icon/linux-logo-icon-reversed.svg)

---

#### Icon Greyscale

![Minimalist Linux Logo - Icon Greyscale](./source-files/vector/Inkscape/Icon/linux-logo-icon-greyscale.svg)

---

## Vector Files

The vector files for the logo can be found in the `./source-files/vector` directory, they are in SVG Inkscape format, they also are available, trimmed, without margin/padding around the logo.

## Bitmap Files

There are bitmap files available in the directory `./source-files/bitmap`. They are available both as PNG and WEBP formats with transparent background.

These are the available sizes, in width:

- 32px (not applicable for Vertical and Horizontal variants)
- 64px
- 128px
- 256px
- 512px
- 1024px
- 2048px

## Credits

The minimalist Linux logo was designed by Clint Quasar.